# vivanno's devOps scripts

A small script collection for beautiful life of IT people.

## usage: 

### docker.sh

install the latest version of docker for ubuntu xenial

* login you with root user on your ubuntu (xenial) server: 
     
        curl https://bitbucket.org/vivanno/devOps/raw/master/docker.sh | bash
	 
* logout /login
* Voila ... it's installed!

### composer-cli.sh

create an composer user and install the latest version of hyperledger composer cli 

* login you with root user on your ubuntu (xenial) server: 

        curl https://bitbucket.org/vivanno/devOps/raw/master/composer-cli.sh | bash

* logout / login 
* su composer
* composer -v
* Voila ... it's installed!