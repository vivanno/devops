apt install -y \
	build-essential \
	ca-certificates \
	curl \
	python \
	software-properties-common

mkdir /home/composer
groupadd composer
useradd -u 12345 -g composer -d /home/composer -s /bin/bash -p $(echo mypasswd | openssl passwd -1 -stdin) composer
usermod -aG docker composer
usermod -aG sudo composer
chown -R composer:composer /home/composer
[ ! -f /home/composer/.bashrc ] && echo "# auto genered bashrc file" > /home/composer/.bashrc
cd /home/composer && \
su composer

curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash

export NVM_DIR="$HOME/.nvm" && \
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" && \
	nvm install --lts && \
   	npm install -g mkdirp \
   	npm install -g composer-cli \
   	npm install -g composer-cli \
	npm install -g composer-rest-server \
	npm install -g generator-hyperledger-composer