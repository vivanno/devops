#!/bin/bash

apt-get update && \
	apt-get upgrade -y
	
apt-get install -y \
	sudo \
	git \
	xfce4 \
	xfce4-* \
	xrdp

service xrdp restart

# add user
# cd home
# echo xfce4-session >~/.xsession
# exit

